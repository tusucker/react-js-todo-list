import React, {Component} from 'react';
import {v4 as uuidv4} from 'uuid';
import _ from 'lodash';
import TaskForm from "../components/TaskForm";
import Controls from "../components/Controls";
import TaskList from "../components/TaskList";

class Master extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animals: JSON.parse(localStorage.getItem('animals')),
            animalEdit: {},
            isRenderForm: false,
            isFormInsert: false,
            isFormUpdate: false
        };
    }

    generateData = () => {
        let animals = [
            {
                id: uuidv4(),
                name: 'Dog',
                description: 'something about dog',
                status: true
            },
            {
                id: uuidv4(),
                name: 'Cat',
                description: 'something about cat',
                status: false
            },
            {
                id: uuidv4(),
                name: 'Pig',
                description: 'something about pig',
                status: true
            },
        ];
        localStorage.setItem('animals', JSON.stringify(animals));
        this.setState({
            animals: JSON.parse(localStorage.getItem('animals'))
        });
    }
    setStatusIsFalse = () => {
        this.setState({
            isRenderForm: false,
            isFormInsert: false,
            isFormUpdate: false
        });
    }
    renderForm = () => {
        if (this.state.isRenderForm && this.state.isFormInsert) {
            return <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <TaskForm name="Insert" closeForm={() => this.setStatusIsFalse()}
                          submitForm={(value) => this.submitForm(value)}/>
            </div>;
        }
        if (this.state.isRenderForm && this.state.isFormUpdate) {
            return <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <TaskForm animalEdit={this.state.animalEdit} name="Update" closeForm={() => this.setStatusIsFalse()}
                          submitForm={(value) => this.submitForm(value)}/>
            </div>;
        }
    }
    setStatusIsTrue = () => {
        this.setState({
            isRenderForm: true,
            isFormInsert: true,
        });
    }

    submitForm = (value) => {
        let {animals} = this.state;
        if (value.id === '') {
            value.id = uuidv4();
            animals.push(value);
        } else {
            animals.forEach((animal) => {
                if (animal.id === value.id) {
                    animal.name = value.name;
                    animal.description = value.description;
                    animal.status = value.status;
                }
            });
        }
        this.setState({
            animals: animals
        });
        localStorage.setItem('animals', JSON.stringify(this.state.animals));
    }
    changeStatus = (id) => {
        let {animals} = this.state;
        animals.forEach((animal) => {
            if (animal.id === id) {
                animal.status = !animal.status;
            }
        });
        localStorage.setItem('animals', JSON.stringify(animals));
        this.setState({
            animals: animals
        });
    }
    updateAnimal = (value) => {
        this.setState({
            isRenderForm: true,
            isFormInsert: false,
            isFormUpdate: true,
            animalEdit: value
        });
    }
    deleteAnimal = (value) => {
        let {animals} = this.state;
        // animals.splice(this.findIndex(value), 1);
        // animals.splice(_.findIndex(animals, (animal) => {
        //     return animal.id === value;
        // }), 1);
        _.remove(animals,(animal)=>{
            return animal.id===value;
        });
        this.setState({
            animals: animals
        });
        localStorage.setItem('animals', JSON.stringify(animals));
    }
    // findIndex = (id) => {
    //     let rs = -1;
    //     this.state.animals.forEach((animal, index) => {
    //         if (animal.id === id) {
    //             rs = index;
    //         }
    //     });
    //     return rs;
    // }

    render() {
        return (
            <div className="container">
                <div className="text-center">
                    <h1>Quản Lý Công Việc</h1>
                    <hr/>
                </div>
                <div className="row">
                    {this.renderForm()}
                    <div
                        className={this.state.isRenderForm ? 'col-xs-8 col-sm-8 col-md-8 col-lg-8' : 'col-xs-12 col-sm-12 col-md-12 col-lg-12'}>
                        <button type="button" className="btn btn-primary mb-5" onClick={() => this.setStatusIsTrue()}>
                            <span className="fa fa-plus mr-5"/>Thêm Công Việc
                        </button>

                        <button type="button" className="btn btn-danger m-3" onClick={() => this.generateData()}>
                            generate data
                        </button>
                        <div className="row mt-5">
                            <Controls/>
                        </div>
                        <div className="row mt-15">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <TaskList
                                    animals={this.state.animals}
                                    changeStatus={(id) => this.changeStatus(id)}
                                    updateAnimal={(value) => this.updateAnimal(value)}
                                    deleteAnimal={(value) => this.deleteAnimal(value)}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Master;