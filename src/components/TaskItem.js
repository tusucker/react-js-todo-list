import React, {Component} from 'react';

class TaskItem extends Component {
    changeStatus=()=>{
        this.props.changeStatus(this.props.animal.id);
    }
    updateAnimal=()=>{
        let {animal}=this.props;
        this.props.updateAnimal(animal);
    }
    deleteAnimal=()=>{
        this.props.deleteAnimal(this.props.animal.id)
    }
    render() {
        let {animal, index} = this.props;
        return (
            <tr>
                <td>{index + 1}</td>
                <td>{animal.name}</td>
                <td>{animal.description}</td>
                <td className="text-center">
                    <div className={animal.status===true?'label label-success':'label label-warning'} onClick={()=>this.changeStatus()}>
                        {animal.status===true?'Kích hoạt':'Chưa kích hoạt'}
                    </div>
                </td>
                <td className="text-center">
                    <button type="button" className="btn btn-warning" onClick={()=>this.updateAnimal()}>
                        <span className="fa fa-pencil mr-5"/>Sửa
                    </button>
                    &nbsp;
                    <button type="button" className="btn btn-danger" onClick={()=>this.deleteAnimal()}>
                        <span className="fa fa-trash mr-5"/>Xóa
                    </button>
                </td>
            </tr>
        );
    }
}

export default TaskItem;