import React, {Component} from 'react';

class TaskForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:this.props.animalEdit?this.props.animalEdit.id:'',
            name:this.props.animalEdit?this.props.animalEdit.name:'',
            description:this.props.animalEdit?this.props.animalEdit.description:'',
            status:this.props.animalEdit?this.props.animalEdit.status:false,
        }
    }

    closeForm = () => {
        this.props.closeForm();
    }
    isChange = (event) => {
        let target = event.target;
        let name = target.name;
        let value = target.value;
        if(name==='status'){
            value=target.value === "true";
        }
        this.setState({
            [name]:value
        });
    }
    submitForm=()=>{
        this.props.submitForm(this.state);
        this.closeForm();
    }

    render() {
        return (
            <div className="panel panel-warning">
                <div className="panel-heading">
                    <h3 className="panel-title">{this.props.name}</h3>
                </div>
                <div className="panel-body">
                    <form>
                        <div className="form-group">
                            <label>Tên :</label>
                            <input type="text" className="form-control" name="name"
                                   value={this.state.name}
                                   onChange={(event) => this.isChange(event)}/>
                        </div>
                        <div className="form-group">
                            <label>Mô tả :</label>
                            <input type="text" className="form-control" name="description"
                                   value={this.state.description}
                                   onChange={(event) => this.isChange(event)}/>
                        </div>
                        <label>Trạng Thái :</label>
                        <select className="form-control"
                                name="status"
                                value={this.state.status}
                                onChange={(event) => this.isChange(event)}>
                            <option value={true}>Kích Hoạt</option>
                            <option value={false}>Ẩn</option>
                        </select>
                        <br/>
                        <div className="text-center">
                            <button type="button" className="btn btn-warning" onClick={()=>this.submitForm()}>{this.props.animalEdit?'Sửa':'Thêm'}</button>
                            &nbsp;
                            <button type="button" className="btn btn-danger" onClick={() => this.closeForm()}>Hủy Bỏ
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default TaskForm;