import React, {Component} from 'react';
import TaskItem from "./TaskItem";

class TaskList extends Component {
    render() {
        let elements = this.props.animals.map((animal, index) => {
            return <TaskItem
                key={animal.id}
                animal={animal}
                index={index}
                changeStatus={this.props.changeStatus}
                updateAnimal={this.props.updateAnimal}
                deleteAnimal={this.props.deleteAnimal}
            />;
        });
        return (
            <table className="table table-bordered table-hover">
                <thead>
                <tr>
                    <th className="text-center">STT</th>
                    <th className="text-center">Tên</th>
                    <th className="text-center">Mô tả</th>
                    <th className="text-center">Trạng Thái</th>
                    <th className="text-center">Hành Động</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td/>
                    <td>
                        <input type="text" className="form-control"/>
                    </td>
                    <td>
                        <input type="text" className="form-control"/>
                    </td>
                    <td>
                        <select className="form-control">
                            <option value="-1">Tất Cả</option>
                            <option value="0">Ẩn</option>
                            <option value="1">Kích Hoạt</option>
                        </select>
                    </td>
                    <td/>
                </tr>
                {elements}
                </tbody>
            </table>
        );
    }
}

export default TaskList;